/*
 * PointCloud.h
 */

#ifndef CLOUD_H_
#define CLOUD_H_

#include <cassert>

#include <pcl/common/common_headers.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "Camera2D.h"

class PointCloud {
public:
	PointCloud();
	virtual ~PointCloud();

	const pcl::PointCloud<pcl::PointXYZRGBA>& cloud();

	bool load(std::string filename);
	bool save(std::string filename);

	void generateFakeData();

	cv::Mat generateImg();
	void restoreColor(const Camera2D& camera);
	void clearColor();

private:
	cv::Matx34f createRt(const cv::Matx33f& R, const cv::Matx31f& t);
	void calcRGB(cv::Point2f pti, const cv::Mat& img, pcl::PointXYZRGBA& ptc);
	void calcRGBNaive(cv::Point2f pti, const cv::Mat& img, pcl::PointXYZRGBA& ptc);

private:
	pcl::PointCloud<pcl::PointXYZRGBA> _cloud;
};

#endif /* CLOUD_H_ */
