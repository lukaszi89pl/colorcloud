/*
 * Camera2D.h
 */

#ifndef CAMERA2D_H_
#define CAMERA2D_H_

#include <cassert>
#include <string>
#include <iostream>
#include <vector>
#include <list>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>

class Camera2D {
public:
	Camera2D();
	virtual ~Camera2D();

	const cv::Mat& img() const;
	const cv::Matx33f& A() const;
	const cv::Matx33f& R() const;
	const cv::Matx31f& t() const;

	void setImg(cv::Mat img); // _img
	void loadImg(std::string filepath);

	void setDefaultRotatiton(); // _R
	void setRotationRPY(float roll = 0, float pitch = 0, float yaw = 0); //_R
	void setRotationVec(float vx = 0, float vy = 0, float vz = 0); // _R

	void setDefaultTranslation();
	void setTranslation(float x = 0, float y =0, float z = 0); // _t

	void setDefaultIntrinsinc(); // _A
	void setIntrinsinc(float fx, float fy, float cx, float cy );
	bool calibrateWithImageSequence(const std::string calibDataPath,
			const std::list<cv::Mat>& calibrationImages); // _A
	bool loadA(const std::string path); // _A
	void saveA(const std::string path); // _A

private:
	bool loadCalibPatternParams(const std::string calibDataPath, cv::Size& pattern_size, cv::Size2f& square_size);

private:
	cv::Mat _img;
	cv::Matx33f _A;
	cv::Matx33f _R;
	cv::Matx31f _t;

};

#endif /* CAMERA2D_H_ */
