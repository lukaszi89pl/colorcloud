/*
 * World3D.cpp
 */

#include "World3D.h"

World3D::World3D() {
	_point_cloud.generateFakeData();
}

World3D::~World3D(){
}

bool World3D::calibrate(std::string images_directory){

	std::list<cv::Mat> mat_list;

	boost::filesystem::path directory (images_directory);
	if ( boost::filesystem::exists(directory) && boost::filesystem::is_directory(directory)){

		boost::filesystem::directory_iterator end_iter;
		for (boost::filesystem::directory_iterator iter(directory); iter != end_iter; ++iter) {
			if (boost::filesystem::is_regular_file(iter->status())) {
				mat_list.push_back(
						cv::imread(iter->path().string(), cv::IMREAD_COLOR));
				if (mat_list.back().empty())
					mat_list.pop_back();
			}
		}
	}


	boost::filesystem::path calib_data = directory / boost::filesystem::path("calib.yaml");
	_camera.calibrateWithImageSequence(calib_data.string(), mat_list);

	return false;
}

bool World3D::loadCloud(std::string filename){
	return _point_cloud.load(filename);
}

bool World3D::saveCloud(std::string filename){
	return _point_cloud.save(filename);
}

bool World3D::genImageFromCloud(){
	_camera.setImg(_point_cloud.generateImg());
	_point_cloud.clearColor();
	return !_camera.img().empty();
}

bool World3D::loadImage(std::string filename){
	_camera.loadImg(filename);
	return !_camera.img().empty();
}

bool World3D::saveImage(std::string filename){
	return cv::imwrite(filename, _camera.img());
}

void World3D::restoreColor(){
	_point_cloud.restoreColor(_camera);
}

void World3D::visualize(){
	_visualizer.run(_point_cloud.cloud());
}
