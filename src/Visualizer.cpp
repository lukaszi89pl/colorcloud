/*
 * Visualizer.cpp
 *
 *  Created on: 31-01-2015
 *      Author: willy
 */

#include "Visualizer.h"

Visualizer::Visualizer() {}

Visualizer::~Visualizer() {}

void Visualizer::run(const pcl::PointCloud<pcl::PointXYZRGBA>& point_cloud){

	const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_ptr = point_cloud.makeShared();

	pcl::visualization::PCLVisualizer viewer("PCL Viewer");

	int v1(0);
	viewer.createViewPort (0.0, 0.0, 0.5, 1.0, v1);
	viewer.setBackgroundColor (0, 0, 0, v1);
	viewer.addText ("viewport1", 10, 10, "v1 text", v1);
	pcl::visualization::PointCloudColorHandlerRGBAField<pcl::PointXYZRGBA> rgba_handler(cloud_ptr);
	viewer.addPointCloud<pcl::PointXYZRGBA> (cloud_ptr, rgba_handler, "rgba", v1);

	int v2(1);
	viewer.createViewPort (0.5, 0.0, 1.0, 1.0, v2);
	viewer.setBackgroundColor (0.1, 0.1, 0.1, v2);
	viewer.addText ("viewport2", 10, 10, "v2 text", v2);
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBA> single_color (cloud_ptr, 0, 255, 0);
	viewer.addPointCloud<pcl::PointXYZRGBA> (cloud_ptr, single_color, "single color", v2);

	viewer.addCoordinateSystem (1.0);


//	viewer.addSphere (cloud->points[0], 0.2, 0.5, 0.5, 0.0, "sphere");


	while (!viewer.wasStopped ()){
	  viewer.spinOnce (100);
	}
}
