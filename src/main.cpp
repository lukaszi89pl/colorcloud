//============================================================================
// Name        : ColorCloud.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "World3D.h"

int main() {
	std::cout << "ColorCloud" << std::endl;
	World3D world;

	world.loadCloud("cup2_points.pcd");
	world.genImageFromCloud();
	world.saveImage("img_init.png");

	world.restoreColor();
	world.visualize();

	world.saveCloud("kinect.pcd");
	world.genImageFromCloud();
	world.saveImage("img_restored.png");

	return 0;
}
