/*
 * IWorld3D.h
 *
 *  Created on: 31-01-2015
 *      Author: willy
 */

#ifndef IWORLD3D_H_
#define IWORLD3D_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>

class IWorld3D {
public:
	IWorld3D();
	virtual ~IWorld3D();

//	virtual void setIntrinsic(float fx, float fy, float cx, float cy) = 0;
//	virtual void loadIntrinsic(std::string filename) = 0;
//	virtual void saveIntrinsic(std::string filename) = 0;
//
//	virtual void setCameraRT() = 0;
//	virtual bool loadCameraRT(std::string filename) = 0;
//	virtual bool saveCameraRT() = 0;

	virtual bool calibrate(std::string images_directory) = 0;

	virtual bool loadCloud(std::string filename) = 0;
	virtual bool saveCloud(std::string filename) = 0;
	virtual bool genImageFromCloud() = 0;
	virtual bool loadImage(std::string filename) = 0;
	virtual bool saveImage(std::string filename) = 0;

	virtual void restoreColor() = 0;
	virtual void visualize() = 0;

};

#endif /* IWORLD3D_H_ */
