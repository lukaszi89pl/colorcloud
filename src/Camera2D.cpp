/*
 * Camera2D.cpp
 */

#include "Camera2D.h"

Camera2D::Camera2D() {

	this->setDefaultIntrinsinc();
	this->setDefaultRotatiton();
	this->setDefaultTranslation();
}

Camera2D::~Camera2D() {
}

const cv::Mat& Camera2D::img() const{
	return _img;
}

const cv::Matx33f& Camera2D::A() const{
	return _A;
}

const cv::Matx33f& Camera2D::R() const{
	return _R;
}

const cv::Matx31f& Camera2D::t() const{
	return _t;
}

void Camera2D::setImg(cv::Mat img) {
	_img = img;
}

void Camera2D::setDefaultRotatiton(){
	_R = cv::Matx33f(9.9984628826577793e-01, 1.2635359098409581e-03, -1.7487233004436643e-02,
			-1.4779096108364480e-03, 9.9992385683542895e-01, -1.2251380107679535e-02,
			1.7470421412464927e-02, 1.2275341476520762e-02,	9.9977202419716948e-01);
}

void Camera2D::setRotationRPY(float roll, float pitch, float yaw){
	assert(false);
//	cv::Mat src = (cv::Mat_<float>(3, 1) << yaw, row, pitch);
//	cv::Mat dst;
//	cv::Rodrigues(src, dst);
//	_R = cv::Matx33f(dst.at<float>(0), dst.at<float>(1), dst.at<float>(2),
//			dst.at<float>(3), dst.at<float>(4), dst.at<float>(5),
//			dst.at<float>(6), dst.at<float>(7), dst.at<float>(8));
}

void Camera2D::setRotationVec(float vx, float vy, float vz){
	cv::Mat src = (cv::Mat_<float>(3, 1) << vx, vy, vz);
	cv::Mat dst;
	cv::Rodrigues(src, dst);
	_R = cv::Matx33f(dst.at<float>(0), dst.at<float>(1), dst.at<float>(2),
			dst.at<float>(3), dst.at<float>(4), dst.at<float>(5),
			dst.at<float>(6), dst.at<float>(7), dst.at<float>(8));
}

void Camera2D::setDefaultTranslation(){
	 _t = cv::Matx31f( 1.9985242312092553e-02, -7.4423738761617583e-04, -1.0916736334336222e-02);
	 //    _t = cv::Matx31f(-2.5558943178152542e-02, 1.0109636268061706e-04, 2.0318321729487039e-03);
}

void Camera2D::setTranslation(float x, float y, float z) {
	_t = cv::Matx31f(x, y, z);
}

void Camera2D::setDefaultIntrinsinc() {
	_A = cv::Matx33f(5.2161910696979987e+02, 0., 3.1755491910920682e+02,
			0.,	5.2132946256749767e+02, 2.5921654718027673e+02,
			0., 0., 1.);
}

void Camera2D::setIntrinsinc(float fx, float fy, float cx, float cy ){
//	float fx_rgb = 5.2921508098293293e+02;
//	float fy_rgb = 5.2556393630057437e+02;
//	float cx_rgb = 3.2894272028759258e+02;
//	float cy_rgb = 2.6748068171871557e+02;

	_A = cv::Matx33f(fx, 0.,cx,
					0.,	fy, cy,
					0., 0., 1.);
}

bool Camera2D::calibrateWithImageSequence(const std::string calibDataPath, const std::list<cv::Mat>& calibrationImages) {
	/*
	 * load calib pattern data
	 */
	cv::Size pattern_size(4, 11); // just some defaults
	cv::Size2f square_size(9.11, 9.11); // just some defaults

	if(!this->loadCalibPatternParams(calibDataPath, pattern_size, square_size))
		return false;

	/*
	 * calc real calib points
	 */
	std::vector<cv::Point3f> pattern_coord_3d;
	for (int i = 0; i < pattern_size.height; i++)
		for (int j = pattern_size.width - 1; j >= 0; j--)
			pattern_coord_3d.push_back(
					cv::Point3f(float(i * square_size.height),
							float((2 * j + (i + 1) % 2) * square_size.width),
							0));

	/*
	 * Find asymmetric circles grid instance on image
	 */
	cv::SimpleBlobDetector::Params blob_params;
	blob_params.maxArea = 10e4;
	cv::SimpleBlobDetector* blob_detector = new cv::SimpleBlobDetector(
			blob_params);
	cv::Ptr<cv::FeatureDetector> feature_detector(blob_detector);

	std::vector<std::vector<cv::Point2f> > image_points;
	for(std::list<cv::Mat>::const_iterator iter = calibrationImages.begin();
			iter  != calibrationImages.end(); ++iter){
		std::vector<cv::Point2f> centers;
		bool pattern_found = cv::findCirclesGrid(*iter,
				pattern_size, centers, cv::CALIB_CB_ASYMMETRIC_GRID,
				feature_detector);

		if (pattern_found)
			image_points.push_back(centers);
	}

	/*
	 * calibration function
	 */
	std::vector<std::vector<cv::Point3f> > object_points(image_points.size(), pattern_coord_3d);
	cv::Size size = calibrationImages.front().size();
	cv::Mat distCoeffs;
	std::vector<cv::Mat> rvecs, tvecs;
	cv::Mat cameraMatrix;
	cv::calibrateCamera(object_points, image_points, size, cameraMatrix, distCoeffs, rvecs, tvecs);

	cameraMatrix.copyTo(_A);
	return true;
}

bool Camera2D::loadCalibPatternParams(const std::string calibDataPath, cv::Size& pattern_size, cv::Size2f& square_size){
	/*
	 * load asymmetric circle grid parameters
	 */
	std::cout << "Loading camera calibration data from file: " << calibDataPath	<< std::endl;
	cv::FileStorage fsr(calibDataPath, cv::FileStorage::READ);
	if (!fsr.isOpened()) {
		std::cerr << "[ERROR] Unable to load calibration data." << std::endl;
		return false;
	}
	assert(fsr.isOpened());

	cv::FileNode node = fsr["pattern_data"];
	if (node.type() == cv::FileNode::NONE) {
		std::cerr << "FileNode: 'pattern_data' is empty" << std::endl;
		assert(node.type() != cv::FileNode::NONE);
	}

	node["pattern_size"] >> pattern_size;
	node["square_size"] >> square_size;
	fsr.release();

	return true;
}

void Camera2D::saveA(const std::string path) {
	cv::Mat cameraMatrix = (cv::Mat_<double>(3, 3) << _A(0, 0), _A(0, 1), _A(0,2),
													_A(1, 0), _A(1, 1), _A(1, 2),
													_A(2, 0), _A(2, 1), _A(2, 2));

	std::cout << "Saving camera intrinsic matrix to file: " << path << std::endl;
	cv::FileStorage fs(path, cv::FileStorage::WRITE);
	assert(fs.isOpened());

	fs << "calibration_data";
	fs << "{";
	fs << "cameraMatrix" << cameraMatrix;
	fs << "}";
	fs.release();
}

bool Camera2D::loadA(const std::string path) {
	std::cout << "Loading camera calibration data from file: " << path	<< std::endl;

	cv::FileStorage fs(path, cv::FileStorage::READ);
	if(!fs.isOpened())
		return false;

	cv::FileNode node = fs["calibration_data"];
	if (node.type() == cv::FileNode::NONE) {
		std::cerr << "FileNode: 'calibration_data' is empty" << std::endl;
		assert(node.type() != cv::FileNode::NONE);
	}

	cv::Mat cameraMatrix;
	node["camera_matrix"] >> cameraMatrix;
	fs.release();

	cameraMatrix.copyTo(_A);
	std::cout << "Camera matrix read from file" << std::endl;

	return true;

}

void Camera2D::loadImg(std::string filepath) {
	_img = cv::imread(filepath, cv::IMREAD_COLOR);
}
