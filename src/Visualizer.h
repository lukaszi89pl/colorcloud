/*
 * Visualizer.h
 *
 *  Created on: 31-01-2015
 *      Author: willy
 */

#ifndef VISUALIZER_H_
#define VISUALIZER_H_

#include <pcl/visualization/pcl_visualizer.h>

class Visualizer {
public:
	Visualizer();
	virtual ~Visualizer();

	void run(const pcl::PointCloud<pcl::PointXYZRGBA>& point_cloud);
};

#endif /* VISUALIZER_H_ */
