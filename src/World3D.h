/*
 * World3D.h
 */

#ifndef WORLD3D_H_
#define WORLD3D_H_

#include "IWorld3D.h"
#include "Camera2D.h"
#include "PointCloud.h"
#include "Visualizer.h"

class World3D : public IWorld3D{
public:
	World3D();
	virtual ~World3D();

	bool calibrate(std::string images_directory);

	bool loadCloud(std::string filename);
	bool saveCloud(std::string filename);

	bool genImageFromCloud();
	bool loadImage(std::string filename);
	bool saveImage(std::string filename);

	void restoreColor();
	void visualize();

private:
	Camera2D _camera;
	PointCloud _point_cloud;
	Visualizer _visualizer;
};

#endif /* WORLD3D_H_ */
