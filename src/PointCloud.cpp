/*
 * PointCloud.cpp
 */

#include "PointCloud.h"

PointCloud::PointCloud(){}

PointCloud::~PointCloud() {}

const pcl::PointCloud<pcl::PointXYZRGBA>& PointCloud::cloud(){
	return _cloud;
}

bool PointCloud::load(std::string filename){
	_cloud.clear();
	pcl::io::loadPCDFile (filename, _cloud);

	for(size_t i(0); i < _cloud.size(); ++i)
		_cloud[i].a = 255;

	return !_cloud.empty();
}

bool PointCloud::save(std::string filename){
	if(_cloud.empty())
		return false;

	return (pcl::io::savePCDFile (filename, _cloud) == 0);
}

void PointCloud::generateFakeData(){
	  _cloud.clear();
	  uint8_t r(255), g(15), b(15);
	  for (float z(-1.0); z <= 1.0; z += 0.05){
		  for (float angle(0.0); angle <= 360.0; angle += 5.0){
			  pcl::PointXYZRGBA point;
			  point.x = 0.5 * cosf (pcl::deg2rad(angle));
			  point.y = sinf (pcl::deg2rad(angle));
			  point.z = z;

			  point.r = r;
			  point.g = g;
			  point.b = b;
			  point.a = 255;

			  _cloud.points.push_back(point);
		  }

		  if (z < 0.0){
			  r -= 12;
			  g += 12;
		  }
		  else{
			  g -= 12;
			  b += 12;
		  }
	  }
	  _cloud.width = (int) _cloud.points.size();
	  _cloud.height = 1;
}

cv::Mat PointCloud::generateImg(){
	if(!_cloud.isOrganized() || _cloud.empty())
		return cv::Mat();

	cv::Mat img( _cloud.height, _cloud.width, CV_8UC3, cv::Scalar(0,0,0) );
	for (int h = 0; h < img.rows; h++)
		for (int w = 0; w < img.cols; w++) {
			pcl::PointXYZRGBA pt = _cloud.at(w, h);
			img.at<cv::Vec3b>(h, w) = cv::Vec3b(pt.b, pt.g, pt.r);
		}
	return img;
}

void PointCloud::restoreColor(const Camera2D& camera){

	cv::Matx34f ARt = camera.A() * this->createRt( camera.R(), camera.t());
	for(size_t i(0); i < _cloud.size(); ++i){
		pcl::PointXYZRGBA* ptc;
		ptc = &_cloud.at(i);
		cv::Matx41f M( ptc->x, ptc->y, ptc->z, 1. );
		cv::Matx31f uv = ARt * M;

		cv::Point2f pti( uv(0) / uv(2), uv(1) / uv(2) );
		this->calcRGB(pti, camera.img(), *ptc);
	}
}

cv::Matx34f PointCloud::createRt(const cv::Matx33f& R, const cv::Matx31f& t){
	cv::Matx34f Rt( R(0,0), R(0,1), R(0,2), t(0),
					R(1,0), R(1,1), R(1,2), t(1),
					R(2,0), R(2,1), R(2,2), t(2) );
	return Rt;
}

void ::PointCloud::clearColor(){
	for(size_t i(0); i < _cloud.size(); ++i){
		_cloud[i].r = 255;
		_cloud[i].g = 255;
		_cloud[i].b = 255;
		_cloud[i].a = 255;
	}
}

void PointCloud::calcRGB(cv::Point2f pti, const cv::Mat& img, pcl::PointXYZRGBA& ptc){
	assert( !img.empty() );
	assert( img.type() == CV_8UC3 );

	float max_col( ceil(pti.x) );
	float min_col( floor(pti.x) );
	float max_row( ceil (pti.y) );
	float min_row( floor(pti.y) );

	// skip all irrelevant points
	if ( max_col >= img.cols || max_row >= img.rows || min_col < 0 || min_row < 0
			|| isnan(max_col) || isnan(min_col) || isnan(max_row) || isnan(min_row) ){
		ptc.rgb = 0;
		ptc.a = (uint8_t)0;
		return;
	}

	assert( max_col <  img.cols );
	assert( max_row <  img.rows );
	assert( max_col >=  0 );
	assert( max_row >=  0 );

	assert( min_col <  img.cols );
	assert( min_row <  img.rows );
	assert( min_col >=  0 );
	assert( min_row >=  0 );

	/*
	 * interpolate color
	 * left top, right top, left bottom and right bottom
	 * [q11 q12]
	 * {q21 q22]
	 */
	cv::Vec3f q11( img.at<cv::Vec3b>( (int)min_row, (int)min_col ) );
	cv::Vec3f q12( img.at<cv::Vec3b>( (int)min_row, (int)max_col ) );
	cv::Vec3f q21( img.at<cv::Vec3b>( (int)max_row, (int)min_col ) );
	cv::Vec3f q22( img.at<cv::Vec3b>( (int)max_row, (int)max_col ) );

	float delta_col( pti.x - min_col );
	float delta_row( pti.y - min_row );

	std::vector<uint8_t> bgr_vec(3);
	for(size_t i=0; i<bgr_vec.size(); ++i){
		// linear interpolation of bottom neighborhoods
		float pixel_val_bottom( (1.-delta_col)*q21[i] + delta_col*q22[i] );

		// linear interpolation of top neighborhoods
		float pixel_val_top( (1.-delta_col)*q11[i] + delta_col*q12[i] );

		// linear interpolation of both sides
		float pixel_val = ( (1.-delta_row)*pixel_val_top + delta_row*pixel_val_bottom );

		bgr_vec[i] = static_cast<uint8_t>( pixel_val );
	}

	ptc.r = bgr_vec[2];
	ptc.g = bgr_vec[1];
	ptc.b = bgr_vec[0];
	ptc.a = 255;
}

void PointCloud::calcRGBNaive(cv::Point2f pti, const cv::Mat& img, pcl::PointXYZRGBA& ptc){

	assert( !img.empty() );
	assert( img.type() == CV_8UC3 );

	cv::Point pt( pti.x, pti.y );

	if( isnan(pti.x) || isnan(pti.y)
			|| pt.x >= img.cols || pt.y >= img.rows || pt.x < 0 || pt.y < 0){
		ptc.rgb = 0.;
		ptc.a = (uint8_t)0;
	}
	else{
		ptc.r = img.at<cv::Vec3b>(pt)[2];
		ptc.g = img.at<cv::Vec3b>(pt)[1];
		ptc.b = img.at<cv::Vec3b>(pt)[0];
		ptc.a = 255;
	}
}
