
Lista bibliotek potrzebnych do uruchomienia programu:
- OpenCV
- Boost
- PCL
- VTK
- Eigen3

linker:
boost_system
boost_filesystem
pcl_common
pcl_io
pcl_visualization
vtkCommon
vtkFiltering
vtkRendering
opencv_core
opencv_imgproc
opencv_highgui
opencv_features2d
opencv_calib3d


include:
/usr/include/pcl-1.7
/usr/include/eigen3
/usr/include/vtk-5.8
